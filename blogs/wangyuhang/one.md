---
title: JS中的this指向问题
date: 2022-02-06
categories:
 - 汪宇航
tags:
 - js
sidebar: 'auto'
---
---

## JS中的this指向问题

解析器在调用函数每次都会向函数内部传递进一个隐含的参数，这个隐含的参数就是this，this指向的是一个对象，这个对象我们称为函数执行的上下文对象，根据函数的调用方式不同，this会指向不同的对象。本文将分享4种this指向情况：
1. 以全局普通函数的形式调用时，this永远都指向window：

```js
 var global = function() {
     console.log(this)  //window
 }
 global()
```
2. 以方法的形式调用时，this指向调用该方法的对象：

```js
 var person = {
     name: Jack,
     sayName(){
         console.log(this)  //person对象
         console.log(this.name) //Jack
     } 
 }
```

3.  以构造函数的形式调用时，this指向new创建的对象实例：

```js
 var Person = function() {
     this.name = Jack,
     this.sayName = function() {
         console.log(this)
     } 
 }
 var person1 = new Person()
 person1.sayName() //person1
```

4. 箭头函数没有自身的this，this指向上级的this:

```js
const person2 = {
    name: Peter,
    sayName() {
        console.log(this)  //person2对象
    }，
    wait() {
        setTimeout(()=> {
            console.log(this)  //person2对象
        }, 1000)
    }
}
```

PS：call()和apply()对this指向的影响：

**当一个函数被call或者apply调用时，this指向传入的对象的值**

```js
 var obj = {
     fn() {
         console.log(this)
     }
 }
 
 obj.fn()  //obj
 obj.fn.call(obj2)  //obj2
```
以上就是JS中this指向的几种情况，需要注意的是在不同场景中this指向什么值是在函数执行的时候确定的，不是在函数定义的时候决定的（对函数执行上下文的理解）。

