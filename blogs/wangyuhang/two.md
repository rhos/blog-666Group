---
title: CSS垂直与水平居中
date: 2022-02-06
categories:
 - 汪宇航
tags:
 - js
sidebar: 'auto'
---
---

## CSS垂直与水平居中

利用 CSS 来实现对象的居中有许多不同的方法，其实居中主要分为了两种类型：**居中元素宽高已知** 和 **居中元素宽高未知**，下面针对这两种类型列举几种常见的方法：

## 居中元素宽高已知：

1. absolute + margin auto

    父元素与当前元素的高度均已设置，父元素设置相对定位，当前元素设置绝对定位，将各个方向的距离都设置为 0，并将 `margin` 设置为 `auto`
   
    
```js
.parent {
        width: 200px;
        height: 200px;
        background-color: skyblue;
        position: relative;
}

.child {
        width: 50px;
        height: 50px;
        background-color: gold;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
}
```

2. absolute + 负 margin

    父元素设置相对定位，当前元素设置绝对定位50%，该百分比是基于相对定位（也就是父元素）来定位的；然后再将 `margin-top` 和 `margin-left` 设置为自身的高度和宽度的负数

```js
.parent {
        width: 200px;
        height: 200px;
        background-color: skyblue;
        position: relative;
}

.child {
        width: 50px;
        height: 50px;
        background-color: gold;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -25px;
        margin-top: -25px;
} 
```
**PS：margin负值**

`margin-top`负值：元素向上移

`margin-left`负值：元素向左移

`margin-bottom`负值：元素本身不变，下边元素上移

`margin-right`负值：元素本身不变，右边元素左移

3. absolute + calc

    父元素设置相对定位，当前元素设置绝对定位，接着使用 CSS3 的一个计算函数，与方法2类似。
    
```js
.parent {
        width: 200px;
        height: 200px;
        background-color: skyblue;
        position: relative;
}

.child {
        width: 50px;
        height: 50px;
        background-color: gold;
        position: absolute;
        top: calc(50% - 25px);
        left: calc(50% - 25px);
} 
```

## 居中元素宽高未知：

1. absolute + transform
    
    父元素设置相对定位，当前元素设置绝对定位50%，利用 CSS3 的新特性 `transform：translate（-50%, -50% ）`，该百分比将是基于自身的宽高计算出来的
    
```js
.parent {
        width: 200px;
        height: 200px;
        background-color: skyblue;
        position: relative;
}

.child {
        background-color: gold;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
}
```

2. line-height + vertical-align

    把当前元素设置为行内元素，然后通过设置父元素的 `text-align: center` 实现水平居中；

    同时通过设置当前元素的 `vertical-align: middle` 来实现垂直居中；

    最后设置当前元素的 `line-height: initial` 来继承父元素的`line-height`
    
    CSS 的属性 vertical-align 用来指定行内元素（inline）,行内块元素(inline-block)或表格单元格（table-cell）元素的垂直对齐方式
    
```js
.parent {
        width: 200px;
        background-color: skyblue;
        line-height: 200px;
        text-align: center;

}

.child {
        background-color: gold;
        display: inline-block; 
        vertical-align: middle;
        line-height: initial;
}
```

3. css-table 表格样式

    把当前元素设置为行内元素，父元素设置为表格样式；设置父元素的 `text-align: center`与`vertical-align: middle`
    
```js
.parent {
        width: 200px;
        height: 200px;
        background-color: skyblue;
        display: table-cell;
        text-align: center;
        vertical-align: middle;  

}

.child {
        background-color: gold;
        display: inline-block;
} 
```

4. flex 布局

    父元素设置为flex布局；设置父元素的 `justify-content: center;`与`align-items: center;`
    
```js
.parent {
        width: 200px;
        height: 200px;
        background-color: skyblue;
        display: flex;
        justify-content: center;
        align-items: center;

}

.child {
        background-color: gold;
} 
```

5. grid布局

    父元素设置为grid布局；设置父元素的 `justify-content: center`与`align-items: center`，与flex布局类似
     
```js
.parent {
        width: 200px;
        height: 200px;
        background-color: skyblue;
        display: grid;
        justify-content: center;
        align-items: center;

}

.child {
        background-color: gold;
} 
```

参考文献：[2021 年 CSS 垂直居中的正确打开方式](<https://mp.weixin.qq.com/s/MG-bqm38FSLyTw9a0LBxlw>)


